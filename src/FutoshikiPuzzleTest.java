import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class FutoshikiPuzzleTest
{
	private FutoshikiPuzzle puzzle;
	private int size = 5;
	private boolean exception;

	@Before
	public void setUp()
	{
		puzzle = new FutoshikiPuzzle(size);
	}

	@Test
	public void testIllegalGridSize()
	{
		exception = false;
		try
		{
			puzzle = new FutoshikiPuzzle(0);
		} catch (IllegalArgumentException e)
		{
			exception = true;
		}
		assertTrue(exception);
		
		exception = false;
		try
		{
			puzzle = new FutoshikiPuzzle(10);
		} catch (IllegalArgumentException e)
		{
			exception = true;
		}
		assertTrue(exception);
	}

	@Test
	public void testSetSquare()
	{
		puzzle.setSquare(0, 0, 1);
		assertEquals(1, puzzle.getSquare(0, 0).getValue());

		puzzle.setSquare(size - 1, size - 1, size - 1);
		assertEquals(size - 1, puzzle.getSquare(size - 1, size - 1).getValue());
	}

	@Test
	public void testLegalSquareValue()
	{
		exception = false;
		try
		{
			puzzle.setSquare(0, 0, size + 1);
		} catch (IllegalArgumentException e)
		{
			exception = true;
		}
		assertTrue(exception);

		exception = false;
		try
		{
			puzzle.setSquare(0, 0, -1);
		} catch (IllegalArgumentException e)
		{
			exception = true;
		}
		assertTrue(exception);
	}

	@Test
	public void testLegalSquareCoords()
	{
		exception = false;
		try
		{
			puzzle.setSquare(size, 0, 1);
		} catch (IndexOutOfBoundsException e)
		{
			exception = true;
		}
		assertTrue(exception);

		exception = false;
		try
		{
			puzzle.setSquare(-1, 0, 1);
		} catch (IndexOutOfBoundsException e)
		{
			exception = true;
		}
		assertTrue(exception);

		exception = false;
		try
		{
			puzzle.setSquare(0, size, 1);
		} catch (IndexOutOfBoundsException e)
		{
			exception = true;
		}
		assertTrue(exception);

		exception = false;
		try
		{
			puzzle.setSquare(0, -1, 1);
		} catch (IndexOutOfBoundsException e)
		{
			exception = true;
		}
		assertTrue(exception);
	}

	@Test
	public void testSetRowConstraint()
	{
		puzzle.setSquare(0, 0, 2);
		puzzle.setSquare(0, 1, 1);
		puzzle.setRowConstraint(0, 0, new GreaterThan(puzzle.getSquare(0, 0), puzzle.getSquare(0, 1)));
		assertEquals(new GreaterThan(puzzle.getSquare(0, 0), puzzle.getSquare(0, 1)).toString(), puzzle.getRowConstraint(0, 0).toString());
		
		puzzle.setSquare(size - 1, size - 2, size - 1);
		puzzle.setSquare(size - 1, size - 1, size);
		puzzle.setRowConstraint(size - 1, size - 2, new LessThan(puzzle.getSquare(size - 1, size - 2), puzzle.getSquare(size - 1, size - 1)));
		assertEquals(new LessThan(puzzle.getSquare(size - 1, size - 2), puzzle.getSquare(size - 1, size - 1)).toString(), puzzle.getRowConstraint(size - 1, size - 2).toString());
	}

	@Test
	public void testSetColumnConstraint()
	{
		puzzle.setSquare(0, 0, 2);
		puzzle.setSquare(1, 0, 1);
		puzzle.setColumnConstraint(0, 0, new GreaterThan(puzzle.getSquare(0, 0), puzzle.getSquare(1, 0)));
		assertEquals(new GreaterThan(puzzle.getSquare(0, 0), puzzle.getSquare(1, 0)).toString(), puzzle.getColumnConstraint(0, 0).toString());
		
		puzzle.setSquare(size - 2, size - 1, size - 1);
		puzzle.setSquare(size - 1, size - 1, size);
		puzzle.setColumnConstraint(size - 2, size - 1, new LessThan(puzzle.getSquare(size - 2, size - 1), puzzle.getSquare(size - 1, size - 1)));
		assertEquals(new LessThan(puzzle.getSquare(size - 2, size - 1), puzzle.getSquare(size - 1, size - 1)).toString(), puzzle.getColumnConstraint(size - 2, size - 1));
	}

	@Test
	public void testFillPuzzle()
	{
		/**
		 * |1|<|2|
		 *  ^   v
		 * |2|>|1|
		 * */
		puzzle = new FutoshikiPuzzle(2);
		puzzle.setSquare(0, 0, 1);
		puzzle.setSquare(0, 1, 2);
		puzzle.setSquare(1, 0, 2);
		puzzle.setSquare(1, 1, 1);
		puzzle.setRowConstraint(0, 0, new LessThan(puzzle.getSquare(0, 0), puzzle.getSquare(0, 1)));
		puzzle.setRowConstraint(1, 0, new GreaterThan(puzzle.getSquare(1, 0), puzzle.getSquare(1, 1)));
		puzzle.setColumnConstraint(0, 0, new LessThan(puzzle.getSquare(0, 0), puzzle.getSquare(1, 0)));
		puzzle.setColumnConstraint(0, 1, new GreaterThan(puzzle.getSquare(0, 1), puzzle.getSquare(1, 1)));
		
		assertEquals(1, puzzle.getSquare(0, 0).getValue());
		assertEquals(2, puzzle.getSquare(0, 1).getValue());
		assertEquals(2, puzzle.getSquare(1, 0).getValue());
		assertEquals(1, puzzle.getSquare(1, 1).getValue());
		assertEquals(new LessThan(puzzle.getSquare(0, 0), puzzle.getSquare(0, 1)).toString(), puzzle.getRowConstraint(0, 0));
		assertEquals(new GreaterThan(puzzle.getSquare(1, 0), puzzle.getSquare(1, 1)).toString(), puzzle.getRowConstraint(1, 0));
		assertEquals(new LessThan(puzzle.getSquare(0, 0), puzzle.getSquare(1, 0)).toString(), puzzle.getColumnConstraint(0, 0));
		assertEquals(new GreaterThan(puzzle.getSquare(0, 1), puzzle.getSquare(1, 1)).toString(), puzzle.getColumnConstraint(0, 1));
	}
	
	@Test
	public void testIsDuplicateTrue()
	{
		puzzle.setSquare(3, 3, 3);
		puzzle.setSquare(3, 4, 3);
		assertFalse(puzzle.isLegal());

		puzzle.setSquare(3, 4, 3);
		puzzle.setSquare(4, 4, 3);
		assertFalse(puzzle.isLegal());
	}

	@Test
	public void testIsDuplicateFalse()
	{
		puzzle.setSquare(0, 0, 1);
		puzzle.setSquare(0, 1, 2);
		assertTrue(puzzle.isLegal());

		puzzle.setSquare(0, 0, 1);
		puzzle.setSquare(1, 0, 2);
		assertTrue(puzzle.isLegal());
	}

	@Test
	public void testBrokenConstraintTrue()
	{
		puzzle.setSquare(0, 0, 1);
		puzzle.setSquare(0, 1, 2);
		puzzle.setRowConstraint(0, 0, new GreaterThan(puzzle.getSquare(0, 0), puzzle.getSquare(0, 1)));
		assertFalse(puzzle.isLegal());

		puzzle.setSquare(0, 0, 2);
		puzzle.setSquare(0, 1, 1);
		puzzle.setRowConstraint(0, 0, new LessThan(puzzle.getSquare(0, 0), puzzle.getSquare(0, 1)));
		assertFalse(puzzle.isLegal());
	}

	@Test
	public void testBrokenConstraintFalse()
	{
		puzzle.setSquare(3, 3, 4);
		puzzle.setSquare(4, 3, 5);
		puzzle.setColumnConstraint(3, 3, new LessThan(puzzle.getSquare(3, 3), puzzle.getSquare(4, 3)));
		assertTrue(puzzle.isLegal());

		puzzle.setSquare(3, 3, 2);
		puzzle.setSquare(4, 3, 1);
		puzzle.setColumnConstraint(3, 3, new GreaterThan(puzzle.getSquare(3, 3), puzzle.getSquare(4, 3)));
		assertTrue(puzzle.isLegal());
	}

	@Test
	public void testToString()
	{
		String grid = "--- --- --- --- ---\n| | | | | | | | | |\n--- --- --- --- ---\n                    \n"
				+ "--- --- --- --- ---\n| | | | | | | | | |\n--- --- --- --- ---\n                    \n"
				+ "--- --- --- --- ---\n| | | | | | | | | |\n--- --- --- --- ---\n                    \n"
				+ "--- --- --- --- ---\n| | | | | | | | | |\n--- --- --- --- ---\n                    \n"
				+ "--- --- --- --- ---\n| | | | | | | | | |\n--- --- --- --- ---\n";
		assertEquals(grid, puzzle.toString());
	}
	
	@Test
	public void testGetProblemsDuplicate()
	{
		puzzle.setSquare(0, 0, 1);
		puzzle.setSquare(0, 1, 1);
		assertEquals("Errors:\nYou cannot insert the same value in a row or column more than once (row: 1).\n", puzzle.getProblems());
		
		puzzle.setSquare(0, 1, 0);
		puzzle.setSquare(1, 0, 1);
		assertEquals("Errors:\nYou cannot insert the same value in a row or column more than once (column: 1).\n", puzzle.getProblems());
	}
	
	@Test
	public void testGetProblemsBrokenConstraint()
	{
		puzzle.setSquare(0, 0, 1);
		puzzle.setSquare(0, 1, 2);
		puzzle.setRowConstraint(0, 0, new GreaterThan(puzzle.getSquare(0, 0), puzzle.getSquare(0, 1)));
		assertEquals("Errors:\nYou must respect row and column constraints (row: 1, columns: 1-2).\n", puzzle.getProblems());
		
		puzzle.setSquare(0, 0, 2);
		puzzle.setSquare(0, 1, 1);
		puzzle.setRowConstraint(0, 0, new LessThan(puzzle.getSquare(0, 0), puzzle.getSquare(0, 1)));
		assertEquals("Errors:\nYou must respect row and column constraints (row: 1, columns: 1-2).\n", puzzle.getProblems());
		
		puzzle.setSquare(0, 1, 0);
		puzzle.setSquare(0, 0, 1);
		puzzle.setSquare(1, 0, 2);
		puzzle.setColumnConstraint(0, 0, new GreaterThan(puzzle.getSquare(0, 0), puzzle.getSquare(1, 0)));
		assertEquals("Errors:\nYou must respect row and column constraints (rows: 1-2, column: 1).\n", puzzle.getProblems());
		
		puzzle.setSquare(0, 0, 2);
		puzzle.setSquare(1, 0, 1);
		puzzle.setColumnConstraint(0, 0, new LessThan(puzzle.getSquare(0, 0), puzzle.getSquare(1, 0)));
		assertEquals("Errors:\nYou must respect row and column constraints (rows: 1-2, column: 1).\n", puzzle.getProblems());
	}
}
