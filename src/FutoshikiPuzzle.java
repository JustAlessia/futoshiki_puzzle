import java.util.HashSet;
import java.util.Iterator;
import java.util.NavigableSet;
import java.util.Random;
import java.util.TreeMap;

/**
 * The FutoshikiPuzzle class initialises the grid and the rows and columns
 * containing the constraints. It takes care of creating and printing out the
 * grid, and of randomly filling it up, which simulates the user's input. Each
 * square, row constraint and column constraint can be set independently if
 * needed. If there is any, the class stores and prints out the errors that make
 * the grid illegal.
 * 
 * @author Alessia Nigretti
 */
public class FutoshikiPuzzle
{
	private static int gridsize;
	private FutoshikiSquare[][] grid;
	private Constraint[][] rowConstraints;
	private Constraint[][] columnConstraints;
	private String[] errors;
	private int numberOfChecks;

	/**
	 * The main method will be used once the user interface is available, and it
	 * will allow the user to set the size of the grid. It randomly fills the
	 * grid up, simulating the user's input and therefore allowing illegalities.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args)
	{
		// program's argument as constructor's parameter
		int size = Integer.parseInt(args[0]);
		setSize(size);
		FutoshikiPuzzle puzzle = new FutoshikiPuzzle(gridsize);
		System.out.println(puzzle);
		new FutoshikiGUI(puzzle);
	}

	/**
	 * Instantiates a new Futoshiki puzzle. The user is only allowed to create a
	 * max 9x9 grid. Otherwise, an exception will be thrown.
	 * 
	 * @param size
	 *            the size of the grid
	 */
	public FutoshikiPuzzle(int size)
	{
		if (size > 1 && size <= 9)
		{
			grid = new FutoshikiSquare[size][size];

			for (int row = 0; row < grid.length; row++)
			{
				for (int col = 0; col < grid.length; col++)
				{
					grid[row][col] = new FutoshikiSquare(row, col, 0);
				}
			}

			rowConstraints = new Constraint[size][size - 1];
			columnConstraints = new Constraint[size - 1][size];
			errors = new String[2]; // number of possible errors
			numberOfChecks = 0;
			fillPuzzle();
		} else
		{
			throw new IllegalArgumentException("The maximum size of the puzzle is 9.");
		}
	}

	/**
	 * Sets the value of each square in the puzzle. An exception is thrown if
	 * the value inserted is less than 0 or greater than the grid length.
	 * 
	 * @param row
	 *            the row of the square whose value has to be set
	 * @param col
	 *            the column of the square whose value has to be set
	 * @param value
	 *            the value to fill the square with
	 */
	public void setSquare(int row, int col, int value)
	{
		if (value >= 0 && value <= grid.length)
		{
			grid[row][col].setValue(value);
		} else
		{
			throw new IllegalArgumentException(
					"The value inserted must be greater than 0 and smaller or equal to the length of the grid.");
		}
	}

	/**
	 * Sets the constraint in the rowConstraints array.
	 * 
	 * @param row
	 *            the row of the constraint that has to be set
	 * @param col
	 *            the column of the constraint has to be set
	 * @param value
	 *            the constraint to fill the blank with
	 */
	public void setRowConstraint(int row, int col, Constraint con)
	{
		rowConstraints[row][col] = con;
	}

	/**
	 * Sets the constraint in the columnConstraints array.
	 * 
	 * @param row
	 *            the row of the constraint that has to be set
	 * @param col
	 *            the column of the constraint has to be set
	 * @param value
	 *            the constraint to fill the blank with
	 */
	public void setColumnConstraint(int row, int col, Constraint con)
	{
		columnConstraints[row][col] = con;
	}

	/**
	 * Fills the grid randomly with numbers and constraints. The method creates
	 * random variables that are passed as arguments of the setSquare,
	 * setRowConstraint and setColumnConstraint methods. GreaterThan and
	 * LessThan constraints are generated at the same time at random positions.
	 */
	public void fillPuzzle()
	{
		fillRandomConstraints();
		insertConstantValue();
	}

	private void reset()
	{
		for (int i = 0; i < grid.length; i++)
		{
			for (int j = 0; j < grid.length - 1; j++)
			{
				setSquare(i, j, 0);
			}
		}

		for (int i = 0; i < rowConstraints.length; i++)
		{
			for (int j = 0; j < rowConstraints.length - 1; j++)
			{
				setRowConstraint(i, j, null);
				setColumnConstraint(j, i, null);
			}
		}
	}

	public void fillRandomConstraints()
	{
		reset();

		Random rand = new Random();

		// row constraints
		int rand1 = 0, rand2 = 0;

		rand1 = rand.nextInt(grid.length);
		rand2 = rand.nextInt(grid.length - 1);

		for (int i = 0; i < grid.length / 2; i++)
		{
			setRowConstraint(rand.nextInt(grid.length), rand.nextInt(grid.length - 1),
					new GreaterThan(grid[rand1][rand2], grid[rand1][rand2 + 1]));
			setRowConstraint(rand.nextInt(grid.length), rand.nextInt(grid.length - 1),
					new LessThan(grid[rand1][rand2], grid[rand1][rand2 + 1]));
		}

		// column constraints
		int rand3 = 0, rand4 = 0;

		rand3 = rand.nextInt(grid.length - 1);
		rand4 = rand.nextInt(grid.length);

		for (int i = 0; i < grid.length / 2; i++)
		{
			setColumnConstraint(rand.nextInt(grid.length - 1), rand.nextInt(grid.length),
					new GreaterThan(grid[rand3][rand4], grid[rand3 + 1][rand4]));
			setColumnConstraint(rand.nextInt(grid.length - 1), rand.nextInt(grid.length),
					new LessThan(grid[rand3][rand4], grid[rand3 + 1][rand4]));
		}
	}

	private void insertConstantValue()
	{
		Random rand = new Random();
		setSquare(rand.nextInt(grid.length), rand.nextInt(grid.length), rand.nextInt(grid.length - 1) + 1);
	}

	/**
	 * Creates a String representation of the grid. The grid is generated by
	 * looping vertical bars and hyphens according to the length of the grid.
	 * The spaces between vertical bars are filled with Futoshiki Squares and
	 * Constraints.
	 * 
	 * @return the String that represents the grid
	 */
	@Override
	public String toString()
	{
		String gridString = "";

		for (int i = 0; i < grid.length; i++)
		{
			// top line of square
			for (int j = 0; j < grid.length; j++)
			{
				if (j < grid.length - 1)
				{
					gridString += "--- ";
				} else if (j == grid.length - 1)
				{
					gridString += "---\n";
				}
			}

			// sides of square
			for (int j = 0; j < grid.length; j++)
			{
				if (j < grid.length - 1)
				{
					if (grid[i][j].getValue() != 0)
					{
						gridString += "|" + grid[i][j] + "|" + getRowConstraint(i, j);
					} else
					{
						gridString += "| |" + getRowConstraint(i, j);
					}
				} else if (j == grid.length - 1)
				{
					if (grid[i][j].getValue() != 0)
					{
						gridString += "|" + grid[i][j] + "|\n";
					} else
					{
						gridString += "| |\n";
					}
				}
			}

			// bottom line of square
			for (int j = 0; j < grid.length; j++)
			{
				if (j < grid.length - 1)
				{
					gridString += "--- ";
				} else if (j == grid.length - 1)
				{
					gridString += "---\n";
				}
			}

			// column constraints
			for (int j = 0; j < grid.length; j++)
			{
				if (i < grid.length - 1 && j < grid.length - 1)
				{
					gridString += " " + getColumnConstraint(i, j) + "  ";
				} else if (i < grid.length - 1 && j == grid.length - 1)
				{
					gridString += " " + getColumnConstraint(i, j) + "  \n";
				}
			}
		}

		return gridString;
	}

	/**
	 * Gets the value of the square that corresponds to the inserted
	 * coordinates.
	 * 
	 * @param row
	 *            the row of the square whose value is being returned
	 * @param col
	 *            the column of the square whose value is being returned
	 * @return the value of the square to be returned
	 */
	public FutoshikiSquare getSquare(int row, int col)
	{
		return grid[row][col];
	}

	/**
	 * Gets the string value of the row constraint that corresponds to the
	 * inserted coordinates.
	 * 
	 * @param row
	 *            the row of the constraint that is being returned
	 * @param col
	 *            the column of the constraint that is being returned
	 * @return the row constraint too be returned
	 */
	public String getRowConstraint(int row, int col)
	{
		if (null != rowConstraints[row][col])
		{
			return rowConstraints[row][col].toString();
		} else
		{
			return " ";
		}
	}

	/**
	 * Gets the string value of the column constraint that corresponds to the
	 * inserted coordinates.
	 * 
	 * @param row
	 *            the row of the constraint that is being returned
	 * @param col
	 *            the column of the constraint that is being returned
	 * @return the column constraint too be returned
	 */
	public String getColumnConstraint(int row, int col)
	{
		if (null != columnConstraints[row][col])
		{
			return columnConstraints[row][col].toString();
		} else
		{
			return " ";
		}
	}

	/**
	 * Checks if the input value is legal.
	 * 
	 * @return true if the input value is legal, false otherwise
	 */
	public boolean isLegal()
	{
		return !isDuplicate() && !brokenConstraint();
	}

	/**
	 * Checks if the input value is duplicate. If the value is duplicate, its
	 * coordinates get stored into an array errors[] of Strings. Coordinates of
	 * illegalities are increased by 1 to be easily recognised by the user.
	 * 
	 * @return true if the input value is duplicate, false otherwise
	 */
	private boolean isDuplicate()
	{
		boolean duplicate = false;
		errors[0] = "";

		HashSet<Integer> rowValidator, colValidator;

		for (int i = 0; i < grid.length; i++)
		{
			rowValidator = new HashSet<Integer>();
			colValidator = new HashSet<Integer>();

			for (int j = 0; j < grid.length; j++)
			{
				if (grid[j][i].getValue() != 0 && !rowValidator.add(grid[j][i].getValue()))
				{
					errors[0] += "(column: " + (i + 1) + ")";
					duplicate = true;
				}

				if (grid[i][j].getValue() != 0 && !colValidator.add(grid[i][j].getValue()))
				{
					errors[0] += "(row: " + (i + 1) + ")";
					duplicate = true;
				}
			}
		}

		return duplicate;
	}

	/**
	 * Checks if the input value breaks any constraint. If the value breaks a
	 * constraint, its coordinates get stored into an array errors[] of Strings.
	 * For every check, it needs to be checked that the adjacent squares do not
	 * break any constraint and both the values are different from 0. The
	 * minimum value must be different from grid.length and the maximum value
	 * must be different from 1. The operation is repeated for both rows and
	 * columns. Coordinates of illegalities are increased by 1 to be easily
	 * recognised by the user.
	 * 
	 * @return true if the input value breaks any constraint, false otherwise
	 */
	private boolean brokenConstraint()
	{
		boolean broken = false;
		errors[1] = "";

		for (int row = 0; row < grid.length; row++)
		{
			for (int col = 0; col < grid.length - 1; col++)
			{
				// check rows
				Constraint rowConstraint = rowConstraints[row][col];

				if (null != rowConstraint)
				{
					if (rowConstraint.toString().equals(">"))
					{
						// (FIRST < SECOND && (FIRST != 0 && SECOND != 0)) ||
						// FIRST == 1 || SECOND == GRID.LENGTH
						if ((grid[row][col].getValue() < grid[row][col + 1].getValue()
								&& (grid[row][col].getValue() != 0 && grid[row][col + 1].getValue() != 0))
								|| grid[row][col].getValue() == 1 || grid[row][col + 1].getValue() == grid.length)
						{
							errors[1] += "(row: " + (row + 1) + ", columns: " + (col + 1) + "-" + (col + 2) + ")";
							broken = true; // constraint has been broken
						}
					} else if (rowConstraint.toString().equals("<"))
					{
						// FIRST > SECOND && (FIRST != 0 && SECOND != 0) ||
						// FIRST == GRID.LENGTH || SECOND == 1
						if ((grid[row][col].getValue() > grid[row][col + 1].getValue()
								&& (grid[row][col].getValue() != 0 && grid[row][col + 1].getValue() != 0))
								|| grid[row][col].getValue() == grid.length || grid[row][col + 1].getValue() == 1)
						{
							errors[1] += "(row: " + (row + 1) + ", columns: " + (col + 1) + "-" + (col + 2) + ")";
							broken = true; // constraint has been broken
						}
					}
				}
			}
		}

		for (int col = 0; col < grid.length; col++)
		{
			for (int row = 0; row < grid.length - 1; row++)
			{
				// check columns
				Constraint columnConstraint = columnConstraints[row][col];

				if (null != columnConstraint)
				{
					if (columnConstraint.toString().equals("v"))
					{
						// FIRST < SECOND && (FIRST != 0 && SECOND != 0) ||
						// FIRST == 1 || SECOND == GRID.LENGTH
						if ((grid[row][col].getValue() < grid[row + 1][col].getValue()
								&& (grid[row][col].getValue() != 0 && grid[row + 1][col].getValue() != 0))
								|| grid[row][col].getValue() == 1 || grid[row + 1][col].getValue() == grid.length)
						{
							errors[1] += "(rows: " + (row + 1) + "-" + (row + 2) + ", column: " + (col + 1) + ")";
							broken = true; // constraint has been broken
						}
					} else if (columnConstraint.toString().equals("^"))
					{
						// FIRST > SECOND && (FIRST != 0 && SECOND != 0) ||
						// FIRST == GRID.LENGTH || SECOND == 1
						if ((grid[row][col].getValue() > grid[row + 1][col].getValue()
								&& (grid[row][col].getValue() != 0 && grid[row + 1][col].getValue() != 0))
								|| grid[row][col].getValue() == grid.length || grid[row + 1][col].getValue() == 1)
						{
							errors[1] += "(rows: " + (row + 1) + "-" + (row + 2) + ", column: " + (col + 1) + ")";
							broken = true; // constraint has been broken
						}
					}
				}
			}
		}

		return broken;
	}

	/**
	 * Generates a string containing all the problems with an illegal puzzle.
	 * Illegal inputs are duplicate values in a row or column or values that
	 * break a row or column constraint. Coordinates of the illegal inputs are
	 * stored in an array of type String. The array has size 2 because it stores
	 * two kinds of errors: isDuplicate and brokenConstraint.
	 * 
	 * @return string containing illegal values and respective coordinates in
	 *         the puzzle
	 */
	public String getProblems()
	{
		String problems = "";

		if (isDuplicate())
		{
			problems += "- You cannot insert the same value in a row or column more than once " + errors[0] + ".\n";
		}

		if (brokenConstraint())
		{
			problems += "- You must respect row and column constraints " + errors[1] + ".\n";
		}

		if (!isDuplicate() && !brokenConstraint())
		{
			problems = "The puzzle is legal.";
		}

		return problems;
	}

	/**
	 * Gets the grid length.
	 *
	 * @return	the grid length
	 */
	public static int getGridLength()
	{
		return gridsize;
	}

	/**
	 * Sets the size of the puzzle.
	 *
	 * @param	size	the new size
	 */
	public static void setSize(int size)
	{
		gridsize = size;
	}

	/**
	 * Checks if the grid is filled with values.
	 *
	 * @return 	true if the grid is full, false otherwise
	 */
	public boolean isFull()
	{
		for (int row = 0; row < grid.length; row++)
		{
			for (int col = 0; col < grid.length; col++)
			{
				if (grid[row][col].toString().equals(""))
				{
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Solve the puzzle through depth-first search.
	 *
	 * @return	true if the puzzle is solved successfully, false otherwise
	 */
	public boolean solve()
	{
		boolean solvable = false;
		int counter = 1;

		if (isLegal())
		{
			for (int row = 0; row < grid.length; row++)
			{
				for (int col = 0; col < grid.length; col++)
				{
					if (getSquare(row, col).getValue() == 0)
					{
						while (counter <= grid.length && !solvable)
						{
							setSquare(row, col, counter);
							solvable = solve();
							counter++;
						}

						if (!solvable)
						{
							setSquare(row, col, 0);
						}

						return solvable;
					}
				}
			}
			solvable = true;
		} else
		{
			return solvable;
		}
		System.out.println(toString());

		return solvable;
	}

	/**
	 * Generates an array of high scores to insert in a JTable.
	 *
	 * @param 	numberOfChecks 	the number of checks
	 * @param 	input 	the name of the player
	 * @return 	the array of high scores
	 */
	public String[][] highScore(int numberOfChecks, String input)
	{
		String highScoreData[][] = new String[5][2];

		TreeMap<Integer, String> map = new TreeMap<>();
		map.put(numberOfChecks, input);
		map.put(2, "Score 1");
		map.put(20, "Score 2");
		map.put(30, "Score 3");
		map.put(10, "Score 4");
		map.put(60, "Score 5");

		NavigableSet<Integer> allScores = map.navigableKeySet();
		Iterator<Integer> iter = allScores.iterator();

		int row = 0;
		while (iter.hasNext() && row < 5)
		{
			Integer score = iter.next();
			highScoreData[row][0] = map.get(score);
			highScoreData[row][1] = score.toString();
			row++;
		}

		return highScoreData;
	}
	
	/**
	 * Increments number of checks.
	 */
	public void incrementChecks()
	{
		numberOfChecks++;
	}
	
	/**
	 * Gets the number of checks.
	 *
	 * @return 	the number of checks
	 */
	public int getChecks()
	{
		return numberOfChecks;
	}
	
	/**
	 * Resets the counter of checks.
	 */
	public void resetChecks()
	{
		numberOfChecks = 0;
	}
}
