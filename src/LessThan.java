/**
 * The LessThan class extends the abstract
 * superclass Constraint and it implements it for
 * 'less than' constraints.
 * 
 * @author Alessia Nigretti
 */
public class LessThan extends Constraint
{
	/**
	 * Instantiates a new 'less than' constraint.
	 *
	 * @param 	first 	the first square to be compared
	 * @param 	second 	the second square to be compared
	 */
	public LessThan(FutoshikiSquare first, FutoshikiSquare second)
	{
		super(first, second);
	}

	/**
	 * Provides a String representation of the 'less than' constraint.
	 * 
	 * @return	the String representation of the constraint
	 */
	@Override
	public String toString()
	{
		if (isRowAdj())
		{
			return "<";
		} else if (isColAdj())
		{
			return "^";
		}

		return " ";
	}
}
