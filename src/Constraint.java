/**
 * The Constraint class initialises the constraints
 * but does not implement them.
 * It provides a basis to build the GreaterThan and LessThan
 * subclasses.
 * 
 * @author Alessia Nigretti
 */
public abstract class Constraint
{
	private FutoshikiSquare first, second;
	private boolean rowAdj, colAdj;

	/**
	 * Constructs a new abstract constraint and checks
	 * if the two squares analysed are adjacent.
	 *
	 * @param 	first 	the first square to be compared
	 * @param 	second 	the second square to be compared
	 */
	public Constraint(FutoshikiSquare first, FutoshikiSquare second)
	{
		this.first = first;
		this.second = second;

		if (first.getRow() == second.getRow()
				&& (first.getCol() + 1 == second.getCol() || first.getCol() - 1 == second.getCol()))
		{
			rowAdj = true;
		} else if (first.getCol() == second.getCol()
				&& (first.getRow() + 1 == second.getRow() || first.getRow() - 1 == second.getRow()))
		{
			colAdj = true;
		} else
		{
			rowAdj = false;
			colAdj = false;
		}
	}
	
	/**
	 * Initialises a String representation of the constraint.
	 * This is needed to provide a string representation of the
	 * constraints in the subclasses.
	 * 
	 * @return	the abstract representation of the constraint
	 */
	abstract public String toString();

	/**
	 * Returns the condition of the two squares analysed.
	 *
	 * @return 	true if the squares are adjacent, false otherwise
	 */
	protected boolean isRowAdj()
	{
		return rowAdj;
	}

	/**
	 * Returns the condition of the two squares analysed.
	 *
	 * @return 	true if the squares are adjacent, false otherwise
	 */
	protected boolean isColAdj()
	{
		return colAdj;
	}

	/**
	 * Gets the first FutoshikiSquare to compare.
	 *
	 * @return 	the first square
	 */
	protected FutoshikiSquare getFirst()
	{
		return first;
	}

	/**
	 * Gets the second FutoshikiSquare to compare.
	 *
	 * @return 	the second square
	 */
	protected FutoshikiSquare getSecond()
	{
		return second;
	}
}
