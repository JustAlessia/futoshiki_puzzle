import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.JTableHeader;

/**
 * The FutoshikiGUI class represents a graphical user interface that
 * allows the user to interact with the Futoshiki Puzzle game.
 * The user is allowed to create a new puzzle and choose its size.
 * The user can insert values to complete the puzzle and at any
 * point check if the puzzle is legal. It is allowed to reset
 * the TextFields and restart the puzzle.
 * It is possible to solve the puzzle through a depth-first search
 * algorithm.
 * An implementation of a high score table is available and the user
 * is allowed to insert his name in the top 5 chart.
 * 
 * @author Alessia Nigretti
 * @version April 2016
 */
public class FutoshikiGUI
{
	private FutoshikiPuzzle puzzle;
	private JFrame frame = new JFrame("Futoshiki Puzzle");
	private JPanel mainPanel = new JPanel();
	private JTextArea display = new JTextArea(1, 1);
	private JTextField[][] grid;
	private String[][] highScoreData = new String[5][2];

	/**
	 * Instantiates a new Futoshiki GUI.
	 * Initialises the highScoreData array of Strings.
	 * Checks if the puzzle generated is solvable and,
	 * if not, generates a new one.
	 *
	 * @param	puzzle	the puzzle
	 */
	public FutoshikiGUI(FutoshikiPuzzle puzzle)
	{
		this.puzzle = puzzle;

		for (int row = 0; row < 5; row++)
		{
			for (int col = 0; col < 2; col++)
			{
				highScoreData[row][col] = "";
			}
		}

		initialiseGrid();

		if (puzzle.solve())
		{
			createWindow();
		} else
		{
			System.out.println("Regenerating");
			new FutoshikiGUI(new FutoshikiPuzzle(FutoshikiPuzzle.getGridLength()));
		}
	}

	/**
	 * Creates the window containing the JFrame and the items in it.
	 */
	public void createWindow()
	{
		// set JMenuBar
		JMenuBar menuBar = new JMenuBar();

		JMenu file = new JMenu("File");
		menuBar.add(file);

		JMenuItem generate = new JMenuItem("Generate New Puzzle");
		JMenuItem exit = new JMenuItem("Exit");
		file.add(generate);
		file.addSeparator();
		file.add(exit);

		JMenu tools = new JMenu("Tools");
		menuBar.add(tools);

		JMenuItem check = new JMenuItem("Check");
		JMenuItem reset = new JMenuItem("Reset");
		JMenuItem solve = new JMenuItem("Solve");
		tools.add(check);
		tools.add(reset);
		tools.add(solve);

		JMenu help = new JMenu("View");
		menuBar.add(help);

		JMenuItem highScores = new JMenuItem("High Scores");
		JMenuItem about = new JMenuItem("About");
		help.add(highScores);
		help.add(about);

		
		// set main JPanel
		Color background = new Color(238, 230, 155);
		mainPanel.setBackground(background);
		mainPanel.setLayout(new GridLayout(grid.length * 2 - 1, grid.length * 2 - 1, 5, 10));
		mainPanel.setBorder(new EmptyBorder(10, 10, 10, 10));

		
		// set up generation of grid constraints in main panel
		generateConstraints();

		
		// create and set right panel
		JPanel rightPanel = new JPanel();
		rightPanel.setBackground(background);
		rightPanel.setLayout(new GridLayout(2, 1));
		rightPanel.setBorder(new EmptyBorder(10, 20, 10, 20));

		
		// create, fill up and set upper panel
		JPanel upperPanel = new JPanel();
		upperPanel.setBackground(background);
		upperPanel.setLayout(new GridLayout(5, 1));
		upperPanel.setBorder(new EmptyBorder(10, 5, 10, 5));

		JButton resetButton = new JButton("Reset");
		JButton generateButton = new JButton("Generate");
		JButton checkButton = new JButton("Check");
		MatteBorder matteBorder = new MatteBorder(1, 1, 1, 1, Color.BLACK);
		Font buttonFont = new Font("TRUETYPE_FONT", Font.BOLD, 20);
		resetButton.setBorder(matteBorder);
		resetButton.setFont(buttonFont);
		generateButton.setBorder(matteBorder);
		generateButton.setFont(buttonFont);
		checkButton.setBorder(matteBorder);
		checkButton.setFont(buttonFont);

		upperPanel.add(resetButton);
		upperPanel.add(new JLabel(""));
		upperPanel.add(generateButton);
		upperPanel.add(new JLabel(""));
		upperPanel.add(checkButton);

		rightPanel.add(upperPanel);

		
		// create and set elements in right panel
		JPanel probsPanel = new JPanel();
		JScrollPane scroll = new JScrollPane(display);

		display.setEditable(false);
		display.setLineWrap(true);
		display.setWrapStyleWord(true);
		display.setFont(new Font("LUCIDA_SANS", Font.PLAIN, 13));
		display.setBorder(new EmptyBorder(5, 5, 5, 5));

		scroll.setBorder(matteBorder);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		scroll.setPreferredSize(new Dimension(180, 180));

		probsPanel.setBackground(background);
		probsPanel.setBorder(new TitledBorder(matteBorder, "Results:"));
		probsPanel.add(scroll);

		rightPanel.add(probsPanel);

		
		// JButtons listeners
		resetButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				reset();
			}
		});

		generateButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				newPuzzle();
			}
		});

		checkButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				checkLegal();
			}
		});

		// file JMenu listeners
		generate.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				newPuzzle();
			}
		});

		exit.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				System.exit(0);
			}
		});

		// tools JMenu listeners
		check.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				checkLegal();
			}
		});

		reset.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				reset();
			}
		});

		solve.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				puzzle.solve();
				updatePuzzle();
			}
		});

		// view JMenu listeners
		highScores.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				openHighScores();
			}
		});

		about.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				JOptionPane.showMessageDialog(null,
						"-----------------------------------------------------------\n"
								+ "                         Futoshiki Puzzle\n"
								+ "-----------------------------------------------------------\n"
								+ "G5067 Further Programming\n" + "Programming Project\nIan Wakeman\n\n"
								+ "Credits: Alessia Nigretti",
						"Credits", JOptionPane.PLAIN_MESSAGE);
			}
		});

		
		// set JFrame
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout());
		frame.setJMenuBar(menuBar);
		frame.add(mainPanel, BorderLayout.CENTER);
		frame.add(rightPanel, BorderLayout.EAST);
		frame.setPreferredSize(new Dimension(620, 500));
		frame.setResizable(false);
		frame.pack();
		frame.setVisible(true);
	}

	/**
	 * Initialises the grid by inserting JTextFields in every position of the 2-D array.
	 * Copies the constant value automatically generated in the puzzle into the GUI.
	 * Sets graphical details.
	 */
	private void initialiseGrid()
	{
		grid = new JTextField[FutoshikiPuzzle.getGridLength()][FutoshikiPuzzle.getGridLength()];

		for (int row = 0; row < grid.length; row++)
		{
			for (int col = 0; col < grid.length; col++)
			{
				grid[row][col] = new JTextField();
				grid[row][col].setBorder(new MatteBorder(1, 1, 1, 1, Color.BLACK));

				if (puzzle.getSquare(col, row).getValue() != 0)
				{
					grid[row][col].setText(puzzle.getSquare(col, row).toString());
					grid[row][col].setEditable(false);
					grid[row][col].setForeground(Color.red);
				}

				grid[row][col].setFont(new Font("TRUETYPE_FONT", Font.BOLD, 120 / grid.length));
				grid[row][col].setHorizontalAlignment(JTextField.CENTER);
			}
		}
	}

	/**
	 * Generates labels to accommodate row and column constraints.
	 */
	private void generateConstraints()
	{
		for (int i = 0; i < grid.length; i++)
		{
			if (i < grid.length - 1)
			{
				for (int j = 0; j < grid.length; j++)
				{
					if (j < grid.length - 1)
					{
						JLabel label = new JLabel(puzzle.getRowConstraint(i, j));
						mainPanel.add(grid[j][i]);
						mainPanel.add(label);
						label.setFont(new Font("TRUETYPE_FONT", Font.BOLD, 20));
						label.setHorizontalAlignment(JTextField.CENTER);
					} else if (j == grid.length - 1)
					{
						mainPanel.add(grid[j][i]);
					}
				}

				for (int j = 0; j < grid.length; j++)
				{
					JLabel label = new JLabel(puzzle.getColumnConstraint(i, j));

					if (j < grid.length - 1)
					{
						mainPanel.add(label);
						mainPanel.add(new JLabel(""));
					} else if (j == grid.length - 1)
					{
						mainPanel.add(label);
					}

					label.setFont(new Font("TRUETYPE_FONT", Font.BOLD, 20));
					label.setHorizontalAlignment(JTextField.CENTER);
				}
			} else if (i == grid.length - 1)
			{
				for (int j = 0; j < grid.length; j++)
				{
					if (j < grid.length - 1)
					{
						JLabel label = new JLabel(puzzle.getRowConstraint(i, j));
						mainPanel.add(grid[j][i]);
						mainPanel.add(label);
						label.setFont(new Font("TRUETYPE_FONT", Font.BOLD, 20));
						label.setHorizontalAlignment(JTextField.CENTER);
					} else if (j == grid.length - 1)
					{
						mainPanel.add(grid[j][i]);
					}
				}
			}
		}
	}

	/**
	 * Resets the user's input in the JTextFields across the GUI.
	 */
	private void reset()
	{
		puzzle.resetChecks();
		
		for (int row = 0; row < grid.length; row++)
		{
			for (int col = 0; col < grid.length; col++)
			{
				if (grid[row][col].isEditable())
				{
					grid[row][col].setText("");
					puzzle.setSquare(col, row, 0);
				}
			}
		}
		
		display.setText("");
	}

	/**
	 * Opens high scores window.
	 */
	private void openHighScores()
	{
		String[] headers = { "Name", "Score" };
		JTable table = new JTable(highScoreData, headers);
		JTableHeader header = table.getTableHeader();
		header.setBackground(Color.lightGray);
		JScrollPane pane = new JScrollPane(table);
		pane.setPreferredSize(new Dimension(300, 103));
		JOptionPane.showMessageDialog(null, pane, "High Scores", JOptionPane.PLAIN_MESSAGE);
	}

	/**
	 * Generates a new puzzle and allows the user to input the size of it.
	 * Values allowed are from 2 to 9. If different values are inserted,
	 * an exception is caught and an error message is shown.
	 */
	private void newPuzzle()
	{
		try
		{
			String input = JOptionPane.showInputDialog(null, "Choose the size of the new puzzle (2-9):",
					"Generate New Puzzle", JOptionPane.QUESTION_MESSAGE);
			int inputValue = Integer.parseInt(input);

			if (inputValue > 1 && inputValue <= 9)
			{
				frame.dispose();
				puzzle = new FutoshikiPuzzle(inputValue);
				FutoshikiPuzzle.setSize(inputValue);
				new FutoshikiGUI(puzzle);
			} else
			{
				throw new IllegalArgumentException();
			}
		} catch (NumberFormatException e)
		{
			JOptionPane.showMessageDialog(null, "You must insert an integer number.", "Invalid Input",
					JOptionPane.ERROR_MESSAGE);
		} catch (IllegalArgumentException e)
		{
			JOptionPane.showMessageDialog(null, "You must insert a number between 2 and 9.", "Invalid Input",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * Checks if the puzzle is legal at an arbitrary point
	 * during the completion of the puzzle.
	 * Displays problems in the problems panel.
	 * If a value different from the ones allowed is inserted,
	 * an exception is caught and an error message is shown.
	 */
	private void checkLegal()
	{
		puzzle.incrementChecks();
		
		for (int row = 0; row < grid.length; row++)
		{
			for (int col = 0; col < grid.length; col++)
			{
				if (!grid[row][col].getText().equals(""))
				{
					try
					{
						int temp = Integer.parseInt(grid[row][col].getText());
						puzzle.setSquare(col, row, temp);
						display.setText(puzzle.getProblems());
					} catch (NumberFormatException e)
					{
						JOptionPane.showMessageDialog(null, "You must insert an integer number.", "Invalid Input",
								JOptionPane.ERROR_MESSAGE);
					} catch (IllegalArgumentException e)
					{
						JOptionPane.showMessageDialog(null,
								"You must insert a number between 0 and " + grid.length + ".", "Invalid Input",
								JOptionPane.ERROR_MESSAGE);
					}
				} else
				{
					puzzle.setSquare(col, row, 0);
					display.setText(puzzle.getProblems());
				}
			}
		}

		if (puzzle.isFull() && puzzle.getProblems().equals("The puzzle is legal."))
		{
			JButton[] buttons = new JButton[2];
			buttons[0] = new JButton("See High Scores");
			buttons[1] = new JButton("Start New Puzzle");

			buttons[0].addActionListener(new ActionListener()
			{
				@Override
				public void actionPerformed(ActionEvent arg0)
				{
					openHighScores();
					JOptionPane.getRootFrame().dispose();
				}
			});

			buttons[1].addActionListener(new ActionListener()
			{
				@Override
				public void actionPerformed(ActionEvent arg0)
				{
					newPuzzle();
					JOptionPane.getRootFrame().dispose();
				}
			});

			JOptionPane.showMessageDialog(null, "Congratulations! You have successfully completed the puzzle.",
					"Puzzle Completed", JOptionPane.INFORMATION_MESSAGE);
			String input = JOptionPane.showInputDialog(null, "Insert your name:", "High Scores",
					JOptionPane.QUESTION_MESSAGE);

			highScoreData = puzzle.highScore(puzzle.getChecks(), input);

			JOptionPane.showOptionDialog(null, "Start a new puzzle?", "Next Step", JOptionPane.YES_NO_OPTION,
					JOptionPane.QUESTION_MESSAGE, null, buttons, 0);
		}
	}

	/**
	 * Updates the GUI with the values that are currently in the puzzle.
	 */
	private void updatePuzzle()
	{
		for (int row = 0; row < grid.length; row++)
		{
			for (int col = 0; col < grid.length; col++)
			{
				grid[row][col].setText(puzzle.getSquare(col, row).toString());
			}
		}
	}
}
