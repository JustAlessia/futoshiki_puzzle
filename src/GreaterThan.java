/**
 * The GreaterThan class extends the abstract
 * superclass Constraint and it implements it for
 * 'greater than' constraints.
 * 
 * @author Alessia Nigretti
 */
public class GreaterThan extends Constraint
{
	/**
	 * Instantiates a new 'greater than' constraint.
	 *
	 * @param 	first 	the first square to be compared
	 * @param 	second 	the second square to be compared
	 */
	public GreaterThan(FutoshikiSquare first, FutoshikiSquare second)
	{
		super(first, second);
	}

	/**
	 * Provides a String representation of the 'greater than' constraint.
	 * 
	 * @return	the String representation of the constraint
	 */
	@Override
	public String toString()
	{
		if (isRowAdj())
		{
			return ">";
		} else if (isColAdj())
		{
			return "v";
		}

		return " ";
	}

}
