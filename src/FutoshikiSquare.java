/**
 * The FutoshikiSquare class creates a single square
 * and stores its value and coordinates.
 * The class is also in charge of printing the square.
 * It is possible to set the value of the square after
 * it has been created and to return it, and also to
 * return row and column coordinates.
 * 
 * @author Alessia Nigretti
 */
public class FutoshikiSquare
{
	private int value;
	private int row, col;

	/**
	 * Instantiates a new Futoshiki square.
	 * Value, row and column coordinates are passed
	 * as a parameter.
	 *
	 * @param	row		the row of the square whose value has to be set
	 * @param 	col		the column of the square whose value has to be set
	 * @param	value	the value to fill the square with
	 */
	public FutoshikiSquare(int row, int col, int value)
	{
		this.row = row;
		this.col = col;
		this.value = value;
	}

	/**
	 * Creates a String representation of the square.
	 * 
	 * @return	the String that represents the square
	 */
	@Override
	public String toString()
	{
		if (value != 0)
		{
			return Integer.toString(value);
		} else
		{
			return "";
		}
	}

	/**
	 * Sets the value of the square.
	 *
	 * @param value the new value of the square
	 */
	public void setValue(int value)
	{
		this.value = value;
	}

	/**
	 * Gets the row coordinate of the square.
	 *
	 * @return 	the row coordinate of the square
	 */
	public int getRow()
	{
		return row;
	}

	/**
	 * Gets the column coordinate of the square.
	 *
	 * @return 	the column coordinate of the square
	 */
	public int getCol()
	{
		return col;
	}

	/**
	 * Gets the value of the square.
	 *
	 * @return 	the value of the square
	 */
	public int getValue()
	{
		return value;
	}
}
